Installing Docker on WSL2
=========================

This page is part of the instruction on how to [install Docker on Windows](README.md) the right way.

If you're only interested in installing Docker inside a WSL2 distribution without any integration with Windows you don't really need this page. Just install Docker using your distribution's package manager.

Table of Contents
-----------------

- [TLDR - Getting Started](#tldr---getting-started)
- [Install Docker Inside a WSL2 Distribution](#install-docker-inside-a-wsl2-distribution)
- [Access the Docker Daemon From Windows](#access-the-docker-daemon-from-windows)
  - [Communicating with the Docker Daemon](#communicating-with-the-docker-daemon)
  - [Choosing a Daemon Socket Type](#choosing-a-daemon-socket-type)
  - [Specifying a Daemon Socket Type](#specifying-a-daemon-socket-type)
    - [Daemon](#daemon)
    - [Client](#client)
- [Set Up Auto Start](#set-up-auto-start)

TLDR - Getting Started
-------------------------

1. Install docker on your WSL2 distribution.

2. Add `"tcp://127.0.0.1:2375"` to the `"hosts"` key in `/etc/docker/daemon.json`. That is:

   ```json
   {
      "hosts": ["unix://", "tcp://127.0.0.1:2375"],
      ...
   }
   ```

3. On your Windows host create a Docker context:

   ```cmd.exe
   docker context create wsl --docker host=tcp://localhost:2375
   ```

4. Use `-c wsl` when you want to operate on the WSL2 Docker instance rather than on the native Windows instance. For example:

   ```cmd
   docker -c wsl run --rm -it hello-world
   ```

   Alternatively to steps (3) and (4), set the `DOCKER_HOST` environment variable to `tcp://localhost:2375`. This is useful in case you're only using the WSL2 daemon.

Install Docker Inside a WSL2 Distribution
-----------------------------------------

This basically amounts to "use your distribution's package manager".

The Docker website has instructions for [CentOS](https://docs.docker.com/engine/install/centos/), [Debian](https://docs.docker.com/engine/install/debian/), [Fedora](https://docs.docker.com/engine/install/fedora/), [Ubuntu](https://docs.docker.com/engine/install/ubuntu/) and [distribution-agnostic instructions from raw binaries](https://docs.docker.com/engine/install/binaries/) which are basically the same as the [instruction for Windows on the main page here](README.md#installation-instructions) above - download zip, extract, launch `dockerd`.

You can even install Docker on the Alpine WSL distributions found in the Microsoft Store. Just `apk add docker`.

At this point you should also configure your Docker daemon how you prefer it:

- Some ideas might be found in the [_Post-installation steps for Linux_](https://docs.docker.com/engine/install/linux-postinstall/) page on the Docker website.
- Perhaps [run Docker in rootless mode](https://docs.docker.com/engine/security/rootless/).

Access the Docker Daemon From Windows
-------------------------------------

### Communicating with the Docker Daemon ###

If you want to control the Docker daemon running in WSL2 using the Windows `docker.exe` rather than opening a WSL2 shell and using the Linux Docker client from there you need to do two things: Expose the Docker daemon to the Windows host and tell the Windows Docker client to communicate to the WSL2 Docker daemon.

But before you can expose the Docker daemon you need to decide how you want to do it. The Docker daemon and client can communicate over several transport mediums, called "daemon sockets", mostly documented on the [`dockerd` reference page](https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-socket-option).

Basically you have three options:

1. Unencrypted unauthenticated HTTP.
2. HTTP over TLS.
3. Over SSH.

### Choosing a Daemon Socket Type ###

Plaintext HTTP is the simplest to setup, but risks anyone with access to network socket controlling the docker daemon. This means any program running on your host, including programs running in other WSL2 instances and the containers themselves.

If you don't mind that, there's no reason not to use plaintext HTTP.

Using TLS secure your connections, but requires too much configuration for local WSL2 scenarios in my opinion. Details can be found on the Docker website: [_Use TLS (HTTPS) to protect the Docker daemon socket_][Docker.com-TLS].

SSH is a lot easier to set up and is a lot more reasonable in this case. For this you should set up and SSH daemon on your WSL2 distribution with public key authentication (Docker won't work with password authentication).

### Specifying a Daemon Socket Type ###

#### Daemon ####

- To use plaintext HTTP add `"tcp://127.0.0.1:2375"` (or `"tcp://0.0.0.0:2375"`) to the `hosts` key in the `/etc/docker/daemon.json` file. For example:

  ```json
  {
     "hosts": ["unix://", "tcp://127.0.0.1:2375"],
  }
     ...
  ```

- To use SSH you don't need to configure Docker in any special way. (You do need to setup public key authentication in your SSH daemon as mentioned above.)

- To use TLS, see the [_Use TLS (HTTPS) to protect the Docker daemon socket_][Docker.com-TLS] page on the Docker website.

#### Client ####

- To specify the Docker socket on the client you should create a Docker context, like this:

  ```cmd.exe
  docker context create wsl --docker host=tcp://  localhost:2375
  ```

  Then you can use the `-c` flag when you want to operate on the WSL2 Docker instance rather than on the native Windows instance. For example:

  ```cmd.exe
  docker -c wsl run --rm -it hello-world
  ```

  - If you're going to perform multiple operation on the WSL2 daemon you can run `docker context use wsl` to make it the active context and then you won't need the `-c` flag for every command.

  - Alternatively you can set the `DOCKER_HOST` environment variable to `tcp://localhost:2375`. This is useful in case you're only using the WSL2 daemon.

  - You can also use the `-H` flag and pass the string, without creating a context.

- To use SSH specify `ssh://user@localhost:port` when creating the context.

- For TLS see the [instructions on the Docker website][Docker.com-TLS].

Set Up Auto Start
-----------------

Even if you install it using your distribution's package manager rather than download the binaries, it won't run "at startup". WSL2 has degenerate init processes (i.e. `systemd` doesn't do what you might expect).

To run the WSL2 Docker daemon automatically at startup, at logon, etc. use your preferred way. Examples include creating a Windows scheduled task or creating a Windows service using something like [NSSM](http://nssm.cc/download).

- For Ubuntu you might use something like: `wsl.exe -d Ubuntu -u root -- service docker start`

- For Alpine you could use something like: `cmd.exe /c "wsl.exe -u root -- dockerd" >NUL <NUL`

In case you're using SSH you should add it to the command, e.g.: `wsl.exe -d Ubuntu -u root -- service docker start; service ssh start`

[Docker.com-TLS]: https://docs.docker.com/engine/security/protect-access/#use-tls-https-to-protect-the-docker-daemon-socket
