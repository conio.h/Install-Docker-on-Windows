#Requires -RunAsAdministrator

Function MakeSure-DirectoryExists {
    [Diagnostics.CodeAnalysis.SuppressMessage('PSUseApprovedVerbs', '', Justification='Internal function')]
    [CmdletBinding(PositionalBinding=$false)]
    Param (
        [Parameter(Mandatory = $true)]
        [String]
        $Name
    )
    
    If (-Not (Test-Path $Name)) {
        New-Item -ItemType Directory -Force -Path $Name
    }
}

Set-Variable -Option Constant -Name PATH_DOCKER_BIN -Value "$Env:ProgramFiles\Docker\"
Set-Variable -Option Constant -Name PATH_LCOW       -Value "$Env:ProgramFiles\Linux Containers"
Set-Variable -Option Constant -Name PATH_DOCKER_CONFIG -Value "$Env:ProgramData\Docker\config\"
Set-Variable -Option Constant -Name PATH_DOCKER_TEMP_DOWNLOAD -Value "$Env:Temp\DockerInstall\"

MakeSure-DirectoryExists -Name $PATH_DOCKER_BIN
MakeSure-DirectoryExists -Name $PATH_DOCKER_CONFIG
MakeSure-DirectoryExists -Name $PATH_DOCKER_TEMP_DOWNLOAD

# Docker
Invoke-WebRequest "https://repos.mirantis.com/win/static/stable/x86_64/docker-latest.zip" -OutFile "$PATH_DOCKER_TEMP_DOWNLOAD\docker-latest.zip"
Expand-Archive "$PATH_DOCKER_TEMP_DOWNLOAD\docker-latest.zip" -DestinationPath "$Env:ProgramFiles\."

$env_path = [Environment]::GetEnvironmentVariable("PATH", [EnvironmentVariableTarget]::Machine) + ";$PATH_DOCKER_BIN"
[Environment]::SetEnvironmentVariable("PATH", $env_path, [EnvironmentVariableTarget]::Machine)

Set-Content -Path "$PATH_DOCKER_CONFIG\daemon.json" -Value `
'{
    "debug": true,
    "exec-opts":["isolation=process"],
    "experimental": true
}'

& "$PATH_DOCKER_BIN\dockerd.exe" --register-service

# buildx
$DockerBuildxURL = Invoke-WebRequest -Uri "https://api.github.com/repos/docker/buildx/releases/latest" | Select-Object -ExpandProperty Content | ConvertFrom-Json | Select-Object -ExpandProperty assets | Where-Object -Property name -like "buildx-*.windows-amd64.exe" | Select-Object -ExpandProperty browser_download_url
Invoke-WebRequest $DockerBuildxURL -OutFile "$PATH_DOCKER_BIN\cli-plugins\docker-buildx.exe"

# Docker Compose
$DockerComposeURL = Invoke-WebRequest -Uri "https://api.github.com/repos/docker/compose/releases/latest" | Select-Object -ExpandProperty Content | ConvertFrom-Json | Select-Object -ExpandProperty assets | Where-Object -Property name -eq "docker-compose-Windows-x86_64.exe" | Select-Object -ExpandProperty browser_download_url
Invoke-WebRequest $DockerComposeURL -OutFile "$PATH_DOCKER_BIN\cli-plugins\docker-compose.exe"

# Docker Scan
$DockerScanURL = Invoke-WebRequest -Uri "https://api.github.com/repos/docker/scan-cli-plugin/releases/latest" | Select-Object -ExpandProperty Content | ConvertFrom-Json | Select-Object -ExpandProperty assets | Where-Object -Property name -eq "docker-scan_windows_amd64.exe" | Select-Object -ExpandProperty browser_download_url
Invoke-WebRequest $DockerScanURL -OutFile "$PATH_DOCKER_BIN\cli-plugins\docker-scan.exe"

# Kubernetes
Invoke-WebRequest "https://storage.googleapis.com/kubernetes-release/release/stable.txt"  | Select-Object -ExpandProperty Content | ForEach-Object { Invoke-WebRequest ("https://storage.googleapis.com/kubernetes-release/release/" + $_ + "/bin/windows/amd64/kubectl.exe") -OutFile "$PATH_DOCKER_BIN\kubectl.exe" }

# LCOW
Invoke-WebRequest "https://github.com/linuxkit/lcow/releases/download/v4.14.35-v0.3.9/release.zip" -OutFile "$PATH_DOCKER_TEMP_DOWNLOAD\release-v4.14.35-v0.3.9.zip"
Expand-Archive "$PATH_DOCKER_TEMP_DOWNLOAD\release-v4.14.35-v0.3.9.zip" -DestinationPath "$PATH_LCOW\."

# Clean up
Remove-Item "$PATH_DOCKER_TEMP_DOWNLOAD" -Recurse -Force
