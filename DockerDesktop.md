Docker Desktop
==============

_Docker Desktop for Windows_ is bad for you.

_Docker Desktop_ has exactly one advantage over installing Docker:

> The Docker Desktop installation includes Docker Engine, Docker CLI client, Docker Compose, Notary, Kubernetes, and Credential Helper.

(Source: [Docker Inc. website](https://docs.docker.com/docker-for-windows/install/#whats-included-in-the-installer))

Installing Docker requires you to separately download Docker (Engine and CLI), Docker Compose, Kubernetes client, etc. and extract them. It's still quicker, by the way, since _Docker Desktop_ is incredibly bloated, but having a one-click install is definitely an advantage.

The downsides are:

1. Size.  
   _Docker Desktop_ takes 3.1GB installed. Docker takes less than 250MB. At 455MB the _Docker Desktop_ **installer** is larger than Linux and Windows Docker packages extracted combined: 232MB (extracted from 85MB) + 196MB (extracted from 60MB) = 428MB.

2. Controlling Windows and Linux containers simultaneously.  
   You can't do that with _Docker Desktop_. You have to switch between them using the annoying tray icon or `DockerCli.exe` (`-SwitchLinuxEngine`, `-SwitchWindowsEngine` and `-SwitchDaemon`) which takes quite some time.  
   Sure, it's on their roadmap, but there's no way to tell when will it be actually done, and the fact that they dared to release it in that way doesn't induce much confidence.

3. The Docker daemon is registered as a service and run on logon and shut down and unregistered on logoff.  
   If you want different users to use a single Docker daemon that runs as long as the machine runs have fun with fighting with _Docker Desktop_ to get this triviality that I can barely call a feature.

   ![Screenshot of _Docker Desktop_ failing to start because another instance is running in a different TS session](images/DockerDesktop-CloseOtherSessions.png)

4. It pollutes and soils your system.  
   For example, if you ask for WSL2 support it creates **two** additional WSL2 distributions: `docker-desktop` and `docker-desktop-data`. What is that anyway? And why?
